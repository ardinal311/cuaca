package com.ardinal.cuaca;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.ardinal.cuaca.api.WeatherService;
import com.ardinal.cuaca.model.WeatherItem;
import com.ardinal.cuaca.model.WeatherResponse;
import com.ardinal.cuaca.request.WeatherRequest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity implements LocationListener {

    public static String BaseUrl = BuildConfig.BASE_URL;
    public static String AppId = "42a824af533bd68cf45ca4fac276dddd";
    protected double latitude;
    protected double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getCoordinates();
//        getWeatherData();
//        createWeather(weather);

    }

    private void getCoordinates() {
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, new String[] {
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    11
            );
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, this);
    }

    protected void getWeatherData() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .baseUrl(BaseUrl)
                .build();

        WeatherService service = retrofit.create(WeatherService.class);
//        WeatherRequest.latitude = String.valueOf(latitude);
//        WeatherRequest.longitude = String.valueOf(longitude);
//        WeatherRequest.appid = AppId;
//        Log.d("TAG", "getWeatherData: " + WeatherRequest.latitude + WeatherRequest.longitude + WeatherRequest.appid);
        WeatherRequest request = new WeatherRequest(
                String.valueOf(latitude),String.valueOf(longitude), String.valueOf(AppId)
        );
        service.getCurrentWeatherData(request.getLatitude(), request.getLongitude(), request.getAppid())
//                .subscribeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WeatherResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(WeatherResponse response) {
                        createWeather(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        Log.d("RESULT", "COMPLETE");
                    }
                });

    }

    private void createWeather(WeatherResponse weather) {
        TextView mLongitude = findViewById(R.id.mText_longitude);
        TextView mLatitude = findViewById(R.id.mText_latitude);
        TextView mWeatherID = findViewById(R.id.mText_weatherID);
        TextView mWeatherMain = findViewById(R.id.mText_weatherMain);
        TextView mWeatherDesc = findViewById(R.id.mText_weatherDesc);
        TextView mWeatherIcon = findViewById(R.id.mText_weatherIcon);
        TextView mBase = findViewById(R.id.mText_base);
        TextView mMainTemp = findViewById(R.id.mText_mainTemp);
        TextView mMainPressure = findViewById(R.id.mText_mainPressure);
        TextView mMainHumidity = findViewById(R.id.mText_mainHumidity);
        TextView mMainTempMin = findViewById(R.id.mText_mainTempMin);
        TextView mMainTempMax = findViewById(R.id.mText_mainTempMax);
        TextView mMainSeaLevel = findViewById(R.id.mText_mainSeaLevel);
        TextView mMainGrandLevel = findViewById(R.id.mText_mainGrandLevel);
        TextView mWindSpeed = findViewById(R.id.mText_windSpeed);
        TextView mWindDeg = findViewById(R.id.mText_windDeg);
        TextView mCloudsAll = findViewById(R.id.mText_cloudsAll);
        TextView mDt = findViewById(R.id.mText_dt);
        TextView mSysMessage = findViewById(R.id.mText_sysMessage);
        TextView mSysCountry = findViewById(R.id.mText_sysCountry);
        TextView mSysSunrise = findViewById(R.id.mText_sysSunrise);
        TextView mSysSunset = findViewById(R.id.mText_sysSunset);
        TextView mTimezone = findViewById(R.id.mText_timezone);
        TextView mID = findViewById(R.id.mText_id);
        TextView mName = findViewById(R.id.mText_name);

        if (weather != null) {
            mLongitude.setText(getWeatherString(weather.getCoord().getLon()));
            mLatitude.setText(getWeatherString(weather.getCoord().getLat()));
            List<WeatherItem> weatherItems = weather.getWeather();
            if (weatherItems != null) {
                for (WeatherItem i:weatherItems) {
                    mWeatherID.setText(getWeatherString(i.getId()));
                    mWeatherMain.setText(getWeatherString(i.getMain()));
                    mWeatherDesc.setText(getWeatherString(i.getDescription()));
                    mWeatherIcon.setText(getWeatherString(i.getIcon()));
                }
            }
            mBase.setText(getWeatherString(weather.getBase()));
            mMainTemp.setText(getWeatherString(weather.getMain().getTemp()));
            mMainPressure.setText(getWeatherString(weather.getMain().getTemp()));
            mMainHumidity.setText(getWeatherString(weather.getMain().getHumidity()));
            mMainTempMin.setText(getWeatherString(weather.getMain().getTempMin()));
            mMainTempMax.setText(getWeatherString(weather.getMain().getTempMax()));
            mMainSeaLevel.setText(getWeatherString(weather.getMain().getSeaLevel()));
            mMainGrandLevel.setText(getWeatherString(weather.getMain().getGrndLevel()));
            mWindSpeed.setText(getWeatherString(weather.getWind().getSpeed()));
            mWindDeg.setText(getWeatherString(weather.getWind().getDeg()));
            mCloudsAll.setText(getWeatherString(weather.getClouds().getAll()));
            mDt.setText(getWeatherString(weather.getDt()));
            mSysMessage.setText(getWeatherString(weather.getSys().getMessage()));
            mSysCountry.setText(getWeatherString(weather.getSys().getCountry()));
            mSysSunrise.setText(getWeatherString(weather.getSys().getSunrise()));
            mSysSunset.setText(getWeatherString(weather.getSys().getSunset()));
            mTimezone.setText(getWeatherString(weather.getTimezone()));
            mID.setText(getWeatherString(weather.getId()));
            mName.setText(getWeatherString(weather.getName()));
        }
    }

    private <T> String getWeatherString(T t) {
        return String.valueOf(t);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = roundDouble(location.getLatitude(), 2);
        longitude = roundDouble(location.getLongitude(), 2);

        Log.d("TAG", String.valueOf(latitude));
        Log.d("TAG", String.valueOf(longitude));

        getWeatherData();
    }

    private Double roundDouble(double value, int places) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
