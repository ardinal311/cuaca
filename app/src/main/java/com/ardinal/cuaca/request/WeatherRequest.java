package com.ardinal.cuaca.request;

import com.google.gson.annotations.SerializedName;

public class WeatherRequest {
    @SerializedName("lat")
    public String latitude;

    @SerializedName("lon")
    public String longitude;

    @SerializedName("appid")
    public String appid;

    public WeatherRequest(String latitude, String longitude, String appid) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.appid = appid;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }
}
