package com.ardinal.cuaca.api;

import com.ardinal.cuaca.model.WeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {
    @GET("/data/2.5/weather")
    Observable<WeatherResponse> getCurrentWeatherData(@Query("lat")String latitude, @Query("lon") String longitude, @Query("appid") String appid);
//    Observable<Response<WeatherResponse>> getCurrentWeatherData(@Body WeatherRequest body);
}
